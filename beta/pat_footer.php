<footer class="page-footer dark bg-primary center-on-small-only wow bounceInUp">
    <div class="container-fluid">
        <div class="row"></div>
    </div>
    <div class="footer-copyright" id="bottom">
        <div class="container-fluid">
            © Coded by: <a href="mailto:patrick@pweb.solutions"> PWeb.Solutions </a>
        </div>
    </div>
</footer>

<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/tether.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/mdb.js"></script>

<script>
    $(function () {
    var option = {
    responsive: true,
    };
   
var data = [
    {
        value: <?php echo($count_errors);?>,
        color:"#F7464A",
        highlight: "#FF5A5E",
        label: "Errors"
    },
    {
        value: <?php echo($count_info);?>,
        color: "#42a5f5",
        highlight: "#42a5f5",
        label: "Information"
    },
      {
        value: <?php echo($count_dalvik);?>,
        color: "green",
        highlight: "green",
        label: "Debug"
    },
      {
        value: <?php echo($count_verbose);?>,
        color: "white",
        highlight: "white",
        label: "Verbose"
    },
     {
        value: <?php echo($count_warning);?>,
        color: "orange",
        highlight: "orange",
        label: "Warning"
    },
]
      
    var ctx = document.getElementById("errorChart").getContext('2d');
    var myPieChart = new Chart(ctx).Pie(data,option);
});

</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-97345234-1', 'auto');
  ga('send', 'pageview');
</script>

<script>
  new WOW().init();
</script>

<div class="modal fade" id="preview" tabindex="-1" role="dialog" aria-labelledby="preview" aria-hidden="true">
    <div class="modal-dialog modal-fluid" role="document">
        <div class="modal-content" style="background-color: #212121;">
            <div class="modal-body">
              <a data-dismiss="modal" aria-label="Close">
               <img src="https://pweb.solutions/tools/resources/adb_tool.PNG" class="img-fluid" />
                </a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade right" id="useful_modal" tabindex="-1" role="dialog" aria-labelledby="useful_modal" aria-hidden="true">
  <div class="modal-dialog modal-full-height modal-right" role="document">

    <div class="modal-content" style="background-color: #212121; color:white; ">

      <div class="modal-header">
        <p class="heading lead" style="color:#33b5e5;">Useful ADB Commands</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <div class="modal-body">
  <h5 style="color:#00C851;">Device Logs</h5>
  <table class="table table-sm" style="background-color: #2E2E2E; font-size:11px;">
    <tbody>
      <tr>
        <td><mark>adb logcat -c</mark></td>
        <td>Clear Buffer</td>
      </tr>
      <tr>
        <td><mark>adb logcat -v time > logs.txt</mark></td>
        <td>Collect logs to a file</td>
      </tr>
    </tbody>
  </table>
  <h5 style="color:#00C851;">Sideload an APK from PC</h5>
  <table class="table table-sm" style="background-color: #2E2E2E; font-size:11px;">
    <tbody>
      <tr>
        <td><mark>adb install app.apk</mark></td>
        <td>From ADB folder</td>
      </tr>
      <tr>
        <td><mark>adb push app.apk /sdcard/Download/</mark></td>
        <td>Copy from ADB folder to device</td>
        <td></td>
      </tr>
      <tr>
        <td><mark>adb shell pm install /sdcard/Download/app.apk</mark></td>
        <td>Install from device</td>
      </tr>
    </tbody>
  </table>
  <h5 style="color:#00C851;">Recording screen</h5>
  <table class="table table-sm" style="background-color: #2E2E2E; font-size:10px;">
    <tbody>
      <tr>
        <td><mark>adb shell screenrecord /sdcard/demo.mp4</mark></td>
        <td>Start recording</td>
      </tr>
      <tr>
        <td><mark>adb pull /sdcard/demo.mp4</mark></td>
        <td>Download the video to ADB folder</td>
      </tr>
    </tbody>
  </table>
  <h5 style="color:#00C851;">Screenshots</h5>
  <table class="table table-sm" style="background-color: #2E2E2E; font-size:11px;">
    <tbody>
      <tr>
        <td><mark>adb shell screencap -p /sdcard/screencap.png</mark></td>
        <td>Take screenshot</td>
      </tr>
      <tr>
        <td><mark>adb pull /sdcard/screencap.png</mark></td>
        <td>Download the screenshot to ADB folder</td>
      </tr>
    </tbody>
  </table>
  <h5 style="color:#00C851;">List Installed Packages to file</h5>
  <table class="table table-sm" style="background-color: #2E2E2E; font-size:11px;">
    <tbody>
      <tr>
        <td><mark>adb shell pm list packages > device_packages.txt</mark></td>
        <td>Current apps/services</td>
      </tr>
      <tr>
        <td><mark>adb shell pm list packages -u > device_packages.txt</mark></td>
        <td>Current & Uninstalled apps/services</td>
      </tr>
    </tbody>
  </table>
  <h5 style="color:#00C851;">Rebooting</h5>
  <table class="table table-sm" style="background-color: #2E2E2E; font-size:11px;">
    <tbody>
      <tr>
        <td><mark>adb reboot</mark></td>
        <td>Reboot device</td>
      </tr>
      <tr>
        <td><mark>adb reboot recovery</mark></td>
        <td>Reboot into Recovery</td>
      </tr>
      <tr>
        <td><mark>adb reboot -bootloader</mark></td>
        <td>Reboot into Bootloader</td>
      </tr>
    </tbody>
  </table>

</div>
</div>
</div>

</div>
</body><html>