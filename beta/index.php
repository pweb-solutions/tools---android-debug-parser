<!DOCTYPE HTML>
<html>
	
<!--
**************************************
Patrick's Logcat Helper


**************************************
-->

	
<?php	
	
/*
error_reporting(E_ALL); 
ini_set('display_errors', 1);
*/
	
	include './pat_header.php';
?>

<body style="background-color: #212121; color:white;">

	<nav class="navbar fixed-top navbar-toggleable-md scrolling-navbar navbar-dark bg-primary">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<a class="navbar-brand" href="#">
			<strong>PWeb ADB Logcat Helper</strong>
		</a>

		<div class="collapse navbar-collapse" id="navbarNav1">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link smooth-scroll" href="#"><i class="fa fa-angle-double-up" aria-hidden="true"></i>Scroll Up</a>
				</li>
				<li class="nav-item">
					<a class="nav-link smooth-scroll" href="#bottom"><i class="fa fa-angle-double-down" aria-hidden="true"></i>Scroll Down</a>
				</li>
					<li class="nav-item">
					<a class="nav-link" data-toggle="modal" data-target="#useful_modal"><i class="fa fa-info-circle animated pulse infinite" aria-hidden="true"></i>Useful Commands</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="https://pweb.solutions/tools/adb/"><i class="fa fa-eraser" aria-hidden="true"></i>Clear/Reset</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/cdn-cgi/l/email-protection#f6869782849f959db686819394d885999a83829f999885"><i class="fa fa-envelope-o" aria-hidden="true"></i>Feedback</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="https://pweb.solutions/tools/"><i class="fa fa-home" aria-hidden="true"></i>Hub</a>
				</li>
			</ul>
		</div>
	</nav>

<br><br><br><br>

<?php 
	if($_FILES==FALSE){ include './pat_first.php'; }
	elseif($_FILES){include './pat_parser.php'; }

	include './pat_footer.php';
?>

	</body>
</html>