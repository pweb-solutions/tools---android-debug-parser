<div class="container">
  
 <div class="row">
    
    <div class="col-md-8">
      <div class="card animated fadeInLeft" style="background-color: #2E2E2E; color:white;">
        <div class="card-block">
					<h4 class="card-title" style="color:#33b5e5;">Extracting Device Logs</h4>
          <p class="card-text">Android device logs (or logcat) can be collected via Android Debug Bridge (ADB).</p>
					<br>
          <h4 class="card-text" style="color:#00C851;">1 - Installation</h4>
          <p class="card-text">The official tool is installed alongside Android Studio but the size of the installer is 1.6GB. Personally, I use a tool created by the community which is only 9MB and only installs what is needed, as opose to the whole SDK. If you are interested
            in using the same tool, then we suggest to install it from <a href="http://forum.xda-developers.com/showthread.php?t=2588979" target="_blank">here</a>.</p>
          <p class="card-text">If you want to use the official Android Studio instead, then please proceed to this <a href="https://developer.android.com/studio/" target="_blank">link</a>.</p>
          <br>
          <h4 class="card-text" style="color:#00C851;">2 - Preparing the device</h4>
          <p class="card-text">
            <ol>
              <li>Navigate to Settings -> About -> Software Info (may vary according to device model)</li>
              <li>Tab 7 times on "Build Number" to activate the hidden "Developer Options" menu</li>
              <li>Back on the main settings, you should now see a menu called "Developer Options"</li>
              <li>Inside this menu, please activate the option "USB Debugging"</li>
            </ol>
          </p>
          <br>
          <h4 class="card-text" style="color:#00C851;">3 - Preparing the connection</h4>
          <p class="card-text">
            <ol>
              <li>Plug your Android device via USB to your Windows machine</li>
              <li>Open the command line and navigate to: <mark>C:\adb</mark></li>
              <li>Type the following command: <mark>adb devices</mark></li>
              <li>Your device should now prompt you to trust this computer</li>
            </ol>
          </p>
			<h4 class="card-text" style="color:#00C851;">4 - Extracting the logs to your Computer</h4>
          <p class="card-text">
            <ol>
              <li>Open your command line and make sure that you are on the folder C:\adb</li>
              <li>Enter the following to clear your buffer: <mark>adb logcat -c</mark></li>
              <li>Enter the following to start extracting logs: <mark>adb logcat -v time > logcat.txt</mark></li>
              <li>Replicate the issue</li>
              <li>Once replicated, use the keyboard combination "Ctrl+c" to close the extraction</li>
              <li>You should now find the file with your logs at c:\adb\logcat.txt</li>
            </ol>
          </p>
        </div> <!-- Card Block -->
      </div> <!-- Card -->
    </div> <!-- Col -->
      
  <div class="col-md-4">
    <div class="card animated fadeInRight" style="background-color: #2E2E2E; color:white;">
      <div class="card-block">
          <form action="" method="post" enctype="multipart/form-data">
            <h4 class="card-title" style="color:#33b5e5;">Parsing the Log File</h4>
						<p class="card-text"> <ol>
              <li>Select your log file with the button below</li>
              <li>Click on Parse!</li>
						</ol>
						</p>
						<input type="file" name="file" size="60" />
						<br><br>
						<input type="submit" value="Parse!" class="btn btn-success btn-rounded btn-sm" />
            </ol>
          </form>
			<hr />
		<a data-toggle="modal" data-target="#preview">Click here for preview!</a>
		

        </div> <!-- Card Block -->
      </div> <!-- Card -->
    </div> <!-- Col -->

  </div> <!-- Row -->

<br>
<div class="row">
	<div class="col-sm-12">
		<div class="card" style="background-color: #2E2E2E; color:white; font-size:10px;">
			<div class="card-block">
				<p class="card-text">Disclaimer: All data is private, confidential and fully encrypted. Results and uploaded file ARE NOT recorded.
				</p>
				</form>
			</div>
		</div>
	</div>
</div>

</div>