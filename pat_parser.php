<?php

/*
error_reporting(E_ALL); 
ini_set('display_errors', 1);
*/

//Checking if file is selected or not
	if($_FILES['file']['name'] != "") {

//Checking if the file is plain text or not
	if(isset($_FILES) && $_FILES['file']['type'] != 'text/plain') {
		echo "<span>File could not be accepted ! Please upload any '*.txt' file.</span>";
		exit();
	} 
	
//Getting and storing the temporary file name of the uploaded file
	$fileName = $_FILES['file']['tmp_name'];
 
//Throw an error message if the file could not be open
	$file = fopen($fileName,"r") or exit("Unable to open file!");
		
// Reading a .txt file line by line
	while(!feof($file)) {
		$line = fgets($file, 4096);
    $linesArray[] = $line; 
	}
	
// Variables Set
	$i = 0;
	$previouslog;
	$previoustime;

// Counters
	$count = count($linesArray);
	$count_info = 0;
	$count_errors = 0;
	$count_dalvik = 0;
	$count_verbose = 0;
	$count_warning = 0;
		
	$month= substr($linesArray[10], 0, 2);
	$day= substr($linesArray[10], 3, 2);

			
While ($i < $count) {
		if ($linesArray[$i] == " " || $linesArray[$i] == "" || $linesArray[$i] == NULL || $linesArray[$i] == 0){ }
		else {
			$resultArray[$i]['Time'] = substr($linesArray[$i], 6, 8);
			$result = substr($linesArray[$i], 19, 2);
			
			if ($result == "W/"){$resultArray[$i]['Type'] ="<font color='orange'><b>Warning: </b></font>"; $count_warning++;}

			elseif ($result == "F/"){
				$resultArray[$i]['Type']="<font color='red'><b>Fatal Error: </b></font>";
				$count_errors++;
			}
			elseif ($result == "E/"){
				$resultArray[$i]['Type']="<font color='red'><b>Error: </b></font>";
				$count_errors++;
			}
			elseif ($result == "I/"){$resultArray[$i]['Type']="<font color='#42a5f5'><b>Information: </b></font>"; $count_info++;}
			elseif ($result == "D/"){$resultArray[$i]['Type']="<font color='green'><b>Debug: </b></font>"; $count_dalvik++;}
			elseif ($result == "V/"){$resultArray[$i]['Type']="<font color='white'><b>Verbose: </b></font>"; $count_verbose++;}
			elseif ($result == "S/"){$resultArray[$i]['Type']="Silent: ";}
			else{$resultArray[$i]['Type']="Other: ";}
			
			$resultArray[$i]['Line'] = substr($linesArray[$i], 21);
			
	// Remove all special words from Line
			$temp_line = $resultArray[$i]['Line'];
			$resultArray[$i]['Line'] = str_replace(array(
				"<div>", 
				"</div>",
				"<body>",
				"</style>",
				"<style",
				"</body>"
			), "", $temp_line);
			
		} // Closing if & else
		$i++;
} //While
		
/*
		echo '<br>Debug:';
		echo '<br>Time: '.$pat_arr[20]['Time'];
		echo '<br>Priority: '.$pat_arr[20]['Priority'];
		echo '<br>Tag: '.$pat_arr[20]['Tag'];
		echo '<br>PID: '.$pat_arr[20]['PID'];
		echo '<br>All Before: ';
		print_r($temp_before);
		echo '<br>All After: ';
		print_r($temp_tag_pid);
		echo '<br>All: ';
		print_r($linesArray[20]);
*/

$i = $i - 2;

echo '
<div class="container-fluid">
	<div class="row">

    <div class="col-md-3">
      <div class="card animated fadeInLeft" style="background-color: #2E2E2E; color:white;">
        <div class="card-block">
          <h4 class="card-title">Report</h4>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td><i class="fa fa-file-text-o" style="color:#aa66cc;" aria-hidden="true"></i> File</td>
                <td>'.$_FILES['file']['name'].'</td>
              </tr>
              <tr>
                <td><i class="fa fa-list" style="color:green;" aria-hidden="true"></i> Parsed</td>
                <td>'; echo(new \DateTime())->format('H:i:s / d-m-Y');
              echo '</td>
              </tr>
              <tr>
                <td><i class="fa fa-calendar" style="color:orange;" aria-hidden="true"></i> Generated</td>
                <td>'.$resultArray[$i]['Time'].' / '.$day.' '.$month.'-2017</td>
              </tr>
              <tr>
                <td><i class="fa fa-info-circle" style="color:#33b5e5;" aria-hidden="true"></i> Size </td>
                <td>'.$count.'</td>
              </tr>
              <tr>
                <td><i class="fa fa-exclamation-triangle animated flash infinite" style="color:red;" aria-hidden="true"></i> Errors </td>
                <td>'.$count_errors.'</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="card" style="background-color: #2E2E2E; color:white;">
        <div class="card-block">
          <h4 class="card-title">Log type distribution</h4>
          <canvas id="errorChart"></canvas>
          <p class="card-text"><font size="2">(Hover to see values)</font></p>
        </div>
      </div>
    </div>
		
		<div class="col-md-3">
      <div class="card animated fadeInRight" style="background-color: #2E2E2E; color:white;">
        <div class="card-block">
          <h4 class="card-title">Legend</h4>
          
          <p class="card-text">Logs are groupped by invocation time, which makes it easier to identify the timestamp.</p>
					<p class="card-text">The following format is being displayed:<br>
					<br><font color="#aa66cc" size="4">Invocation Time (in seconds)</font>
					<br><font color="green"><b>Priority</b></font>
					<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Process Tag (ID): log line</p>

        </div>
      </div>
    </div>
    
  </div>
	<br>
	
';

$i=0;
$count_resultArray = count($resultArray);
reset($resultArray);
		
While ($i < $count_resultArray ) {
	
	if ($resultArray[$i] == " " || $resultArray[$i] == "" || $resultArray[$i] == NULL || $resultArray[$i] == 0){ }
	else {
		
		if ($resultArray[$i]['Time'] <> $previoustime){
			echo '<hr color="#4B515D" /><b><h5 style="color:#aa66cc;">'.$resultArray[$i]['Time'].'</h5></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$previouslog="X";
		}
		else {echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";}
		
		if ($resultArray[$i]['Type'] <> $previouslog){
			echo "<br>".$resultArray[$i]['Type']; 
			echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		
	  echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">'.$resultArray[$i]['Line'].'</font><br>';
		
		$previoustime= $resultArray[$i]['Time'];
		$previouslog= $resultArray[$i]['Type'];

	} // else

$i++;
} // While
		
	echo '</div>';
		
	fclose($file);
} //if($_FILES['file']['name'] != "")

else {
	if(isset($_FILES) && $_FILES['file']['type'] == '')
	echo "File could not be accepted ! Please upload any '*.txt' file.";
}